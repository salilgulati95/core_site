<?php
   
    session_start();
     require 'header.php';
   echo $_GET['$mes'];
?>
<html>
<head>
		<title>LOGIN FORM</title>
		
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bttn.css/0.2.4/bttn.css">
		<link rel="stylesheet" href="css/style.css">

		
		
	</head>
	<body>

		<form name="frm" action="action.php" method="post">
		<div class="container">
			<h1 class="text-center bounceInRight"  style="color:#1d89ff;">LOGIN FORM</h1><br>
			<div class="col-md-1 col-sm-1 animated1 bounceInUp">
					<a href="student_data.php"><i class="fa fa-database" aria-hidden="true" style="font-size:50px;color:#1d89ff;"></i></a>
			</div>
			<div class="col-md-11 col-sm-11">
			<table class="table table-striped">
				<tr class="bounceInLeft animated">
					<td>Email</td>
					<td colspan="2"><input type="email" name="email" placeholder="Enter your Email" class="form-control" required></td>
				</tr>
				<tr class="bounceInRight animated1">
 					<td>Password</td>
					<td colspan="2"><input type="password" name="pass" placeholder="Enter your Password" class="form-control" required></td>
				</tr>
				<tr class="bounceInLeft animated">
					<td colspan="3"><button type="submit" name="btn" class="form-control bttn-unite bttn-primary">LOGIN</button></td>
				</tr>

				</table>
					</div>
		</div>
		</form>
	</body>
</html>
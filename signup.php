<?php
   require 'connection/db.php';
   require 'header.php';

   if(isset($_POST['btn']))
      {
      
        !$file_tmp =$_FILES['file']['tmp_name'];
          $errors= array();
          $file_name = $_FILES['file']['name'];
          $extensions= array("jpeg","jpg","png");
	       if(in_array($file_name,$extensions)=== false)
	       {
	          $errors[]="extension not allowed, please choose a JPEG or PNG file.";
	      	}

            $value1= $_POST['name'];
	            if (!preg_match("/^[a-zA-Z ]*$/",$value1)) 
	            {
				  $value1Err = "Only letters and white space allowed";
				  echo $value1Err;
				}
			$value2=$_POST['email'];
				if (!filter_var($value2, FILTER_VALIDATE_EMAIL))
				 {
				    $value2Err = "Invalid email format";
				    echo $value2Err;
				  }
			$value3=md5($_POST['pass']);
			$value4=$_POST['contact'];
				if (!preg_match ("/^[0-9]*$/", $value4) )
				{  
	    			$value4Err = "Only numeric value is allowed."; 
	    			echo $value4Err;  
	    		}

			$value5=$_FILES['file']['tmp_name'];
			$value6='1';
			$value7=date("Y-m-d");
			
        	$profile = "images/".$file_name;
        	
			 move_uploaded_file($file_tmp,"images/".$file_name);

	         $query = "INSERT INTO form (full_name,email,password,contact_no,profile_file,state_id,created_on) VALUES ('$value1', '$value2', '$value3', '$value4','$profile','$value6','$value7')";
	         $sql=mysqli_query($conn,$query);
			
				if ($conn->query($query) === TRUE) 
			 		{
					 echo"<script>alert('Signup successfully')</script>";
					 header("Location: login.php");
					}
				else 
					  {
					   	echo "Error: " . $sql . "<br>" . $conn->error;
					  }
  
      }
$conn->close();
?>

<html>
	<head>
		<title>SIGNUP FORM</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bttn.css/0.2.4/bttn.css">	
		 <link rel="stylesheet" href="css/style.css">

	</head>
	<body>

		<form name="frm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype = "multipart/form-data">
		<div class="container">
			<h1 class="text-center bounceInRight"  style="color:#1d89ff;">SIGNUP FORM</h1><br>
			<div class="col-md-1 col-sm-1 animated1 bounceInUp">
					<a href="#"><i class="fa fa-database" aria-hidden="true" style="font-size:50px;color:#1d89ff;"></i></a>
			</div>
			<div class="col-md-11 col-sm-11">
			<table class="table table-striped">
            <tr class="bounceInLeft animated">
               <td>Name</td>
               <td colspan="2"><input type="text" name="name" placeholder="Enter your Full Name" class="form-control" required></td>
            </tr>

				<tr class="bounceInRight animated">
					<td>Email</td>
					<td colspan="2"><input type="email" name="email" placeholder="Enter your Email" class="form-control" required></td>
				</tr>
				<tr class="bounceInLeft animated1">
 					<td>Password</td>
					<td colspan="2"><input type="password" name="pass" placeholder="Enter your Password" class="form-control" required></td>
				</tr>
            <tr class="bounceInRight animated">
               <td>Contact</td>
               <td colspan="2"><input type="tel" name="contact" placeholder="Enter your contact no." class="form-control" required></td>
            </tr>
				<tr class="bounceInLeft animated">
					<td>Profile File</td> 
					<td colspan="2"><input type="file" name="file" placeholder="Select file" class="form-control" required></td>
				</tr>
				
			
				<tr class="bounceInRight animated">
					<td colspan="3"><button type="submit" name="btn" class="form-control bttn-unite bttn-primary">SIGNUP</button></td>
				</tr>
			</table>
					</div>
		</div>
		</form>
	</body>
</html>


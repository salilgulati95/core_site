
<html>
<head>
  
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="topnav">
    <a class="navbar-brand" href="#">
          <img src="images/logo.png" alt="logo">
        </a>
      
       <a class="nav-item" href="signup.php">Signup</a>
        <?php if (isset($_SESSION['user']) && $_SESSION['user'] == true): ?>
       <a class="nav-item" href="logout.php">Logout</a>
          <?php else: ?>
       <a class="nav-item" href="login.php">Login</a> 
          <?php endif; ?>   
</div>
</body>
</html>
